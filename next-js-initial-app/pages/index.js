import Head from 'next/head';
import styles from '../styles/Home.module.scss';

export default function Home() {
    return (
        <div className={styles.container}>
            <Head>
                <title>Docthub Next.js App</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                {' '}
                <h1> Welcome to the Docthub Next.js app's intial setup!</h1>
                <h2>App is configured with</h2>
                <ul>
                    <li>Sass</li>
                    <li>Eslint</li>
                    <li>Prettier</li>
                </ul>
            </main>
        </div>
    );
}
