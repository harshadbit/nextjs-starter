import Layout from '../components/Layout';

export default function Home() {
    return (
        <Layout title="DoctHub - Empowering Healthcare Fraternity">
            <div className="divider footer-divider"></div>
        </Layout>
    );
}
