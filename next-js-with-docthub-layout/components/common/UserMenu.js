import { UserTitleEnum } from '../../utils/enums';

export const userMenu = (user, handleLogOut) => {
    const { name = '', title = '' } = user;
    const initial = name ? name.charAt(0) : '';

    return {
        nameInitial: initial,
        fullName: `${UserTitleEnum[title]} ${name}`,
        userMenuLinks: [
            {
                userLinkHeading: '',
                links: [
                    {
                        link: (
                            <a
                                className="doct-user-link"
                                href={`${process.env.NEXT_PUBLIC_IDENTITY_APP}/profile`}>
                                My Profile
                            </a>
                        )
                    }
                ]
            },
            {
                userLinkHeading: 'Saved',
                links: [
                    {
                        link: (
                            <a
                                className="doct-user-link"
                                href={`${process.env.NEXT_PUBLIC_IDENTITY_APP}/profile`}>
                                My Saved Search
                            </a>
                        )
                    },
                    {
                        link: (
                            <a
                                className="doct-user-link"
                                href={`${process.env.NEXT_PUBLIC_IDENTITY_APP}/profile`}>
                                My Orders
                            </a>
                        )
                    },
                    {
                        link: (
                            <a className="doct-user-link" href="" onClick={handleLogOut}>
                                Logout
                            </a>
                        )
                    }
                ]
            }
        ]
    };
};
