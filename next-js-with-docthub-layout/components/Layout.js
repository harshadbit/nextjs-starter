import React, { useState, useEffect } from 'react';
import dynamic from 'next/dynamic';
import Head from 'next/head';

import { userMenu } from './common/UserMenu';
import { useRouter } from 'next/router';

const DoctTopBar = dynamic(() => import('@doct-react/app').then((mod) => mod.DoctTopBar), {
    ssr: false
});

const DoctFooter = dynamic(() => import('@doct-react/ui').then((mod) => mod.DoctFooter), {
    ssr: false
});

const DoctPrivacyPolicy = dynamic(
    () => import('@doct-react/app').then((mod) => mod.DoctPrivacyPolicy),
    { ssr: false }
);

const DoctTermsOfService = dynamic(
    () => import('@doct-react/app').then((mod) => mod.DoctTermsOfService),
    { ssr: false }
);

export default function Layout({ children, title, className }) {
    const [user, setUser] = useState({});
    const [loading, setLoading] = useState(true);
    const [privacyModalOpen, setPrivacyModalOpen] = useState(false);
    const [termsModalOpen, setTermsModalOpen] = useState(false);
    const router = useRouter();

    const handleLogOut = () => {
        import('@doct-react/core').then((mod) => {
            mod.OnLogOut('course').then((res) => {
                setUser({});
                setLoading(false);
            });
        });
    };

    // Google Analytics Implementation
    useEffect(() => {
        if (!window.GA_INITIALIZED) {
            import('@doct-react/core').then((mod) => mod.DoctInitGA());
            window.GA_INITIALIZED = true;
        }
        import('@doct-react/core').then((mod) => mod.DoctLogPageView());
    }, []);

    const allowRobots = process.env.NEXT_PUBLIC_BUILD_ENV === 'production';

    const onDoctLogoClick = () => router.push('/');

    const description = 'Search Healthcare Colleges, Courses, Fellowships globally';
    const keywords =
        'Fellowship, Healthcare course, Observership, Nursing college, Medical college, Dental college, Pharmacy college, Physiotherapy college, Paramedical college, Hospital administration course';

    return (
        <>
            <Head>
                {process.env.NEXT_PUBLIC_BUILD_ENV === 'production' && (
                    <>
                        <meta
                            property="og:title"
                            content="DoctHub | Empowering Healthcare Fraternity"
                        />
                        <meta property="og:description" content="" />
                        <meta property="og:type" content="website" />
                        <meta property="og:url" content="" />
                        <meta property="og:site_name" content="DoctHub | Jobs" />
                        <meta
                            property="og:image"
                            content="https://www.docthub.com/static/media/docthub-d-logo.png"
                        />
                        <meta property="og:image:type" content="image/png" />
                        <meta property="og:image:width" content="200" />
                        <meta property="og:image:height" content="200" />
                        <meta property="og:image:alt" content="DoctHub Logo" />
                        <meta name="keywords" content={keywords} />
                        <meta name="description" content={description} />
                    </>
                )}
                <title>{title || 'Jobs | DoctHub'}</title>
                <link rel="icon" href="/favicon.ico" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="theme-color" content="#00a0c0" />
                {!allowRobots && <meta name="robots" content="noindex,nofollow" />}
                <script src={`/js/app.${process.env.NEXT_PUBLIC_BUILD_ENV}.js`} defer />
            </Head>
            <DoctTopBar userMenu={userMenu(user, handleLogOut)} onDoctLogoClick={onDoctLogoClick} />
            <main className={`bg-grey-200${className ? ` ${className}` : ''}`}>{children}</main>
            <DoctFooter
                onPrivacyHandler={() => setPrivacyModalOpen(!privacyModalOpen)}
                onTermsHandler={() => setTermsModalOpen(!termsModalOpen)}
            />
            <DoctPrivacyPolicy
                open={privacyModalOpen}
                handleClose={() => setPrivacyModalOpen(false)}
            />
            <DoctTermsOfService
                open={termsModalOpen}
                handleClose={() => setTermsModalOpen(false)}
            />
        </>
    );
}
