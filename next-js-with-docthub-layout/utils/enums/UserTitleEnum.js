export const UserTitleEnum = Object.freeze({
    Dr: 0,
    Mr: 1,
    Miss: 2,
    Mrs: 3,
    0: 'Dr.',
    1: 'Mr.',
    2: 'Miss.',
    3: 'Mr.'
});
