window.DoctUrls = {
    doct_brand_url: 'https://docthub.com',
    doct_event_url: 'https://events.docthub.com',
    doct_counselling_url: 'https://counselling.docthub.com',
    doct_jobs_url: 'https://jobs.docthub.com',
    doct_account_url: 'https://accounts.docthub.com/'
};

window.GATrackingId = '';
