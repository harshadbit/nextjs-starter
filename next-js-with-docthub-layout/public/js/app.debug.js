window.DoctUrls = {
    doct_brand_url: 'https://qa.docthub.com',
    doct_event_url: 'https://qa-events.docthub.com',
    doct_counselling_url: 'https://qa-counselling.docthub.com',
    doct_jobs_url: 'https://qa-jobs.docthub.com',
    doct_account_url: 'http://localhost:3001'
};

window.GATrackingId = '';
